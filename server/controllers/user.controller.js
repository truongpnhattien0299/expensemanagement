import User from "../models/user.model.js";

export const getUsers = async (req, res) => {
  try {
    const users = await User.find({}, null, { user: "23123123" });
    res.status(200).json(users);
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: err });
  }
};

export const createUser = async (req, res) => {
  try {
    const newUser = req.body;
    const user = new User(newUser);
    await user.save();
    res.status(200).json(user);
  } catch (error) {
    res.status(500).json({ error });
  }
};

export const updateUser = async (req, res) => {
  try {
    const updateUser = req.body;
    const user = await User.findOneAndUpdate(
      { _id: updateUser._id },
      updateUser,
      { new: true }
    );
    res.status(200).json(user);
  } catch (error) {
    res.status(500).json({ error });
  }
};

export const deleteUser = async (req, res) => {
  try {
    const deleteUser = req.body;
    const user = await User.deleteOne({ _id: deleteUser._id });
    res.status(200).json(user);
  } catch (error) {
    res.status(500).json({ error });
  }
};
