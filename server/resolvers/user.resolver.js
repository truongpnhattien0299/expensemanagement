export default {
  Query: {
    users: async (_, params, { services }) => await services.users.getAllUser(),
  },
  Mutation: {
    createUser: async (_, { input }, { services }) =>
      await services.users.createUser(input),
    updateUser: async (_, { _id, ...data }, { services }) =>
      await services.users.updateUser({ _id, ...data }),
  },
};
