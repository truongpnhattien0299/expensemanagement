import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import dotenv from "dotenv";
import users from "./routers/user.routers.js";
import { ApolloServer } from "apollo-server-express";
import { makeExecutableSchema } from "@graphql-tools/schema";
import schemas from "./schemas/index.js";
import resolvers from "./resolvers/index.js";
import { authorizePlugin } from "./plugins/index.js";
import modelLoader from "./models/index.js";
import serviceLoader from "./services/index.js";

mongoose.plugin(authorizePlugin);

const models = modelLoader();
const services = serviceLoader(models);
dotenv.config();

const app = express();
const PORT = process.env.PORT || 5001;
const LIMIT = process.env.LIMIT || "30mb";
const URI = process.env.URI || "";

app.use(bodyParser.json({ limit: LIMIT }));
app.use(bodyParser.urlencoded({ extended: true, limit: LIMIT }));
app.use(cors());

app.use("/users", users);

mongoose
  .connect(URI)
  .then(() => {
    console.log("Connected to DB");

    const schema = makeExecutableSchema({
      typeDefs: schemas,
      resolvers: Object.values(resolvers),
    });
    const server = new ApolloServer({
      schema,
      resolvers,
      context: () => ({ services: { ...services } }),
    });
    server.start().then(() => server.applyMiddleware({ app }));

    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
    });
  })
  .catch((err) => {
    console.log("err", err);
  });
