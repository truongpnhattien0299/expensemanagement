import baseService from "./base.service.js";

class userService extends baseService {
  constructor(models) {
    super(models);
  }
  getAllUser = async () =>
    await this.models.userModel.find({}, null, {
      user: "ádasd",
      name: "23213213",
    });

  createUser = async (data) => {
    const newUser = data;
    const user = new this.models.userModel(newUser);
    await user.save();
    return user;
  };
  updateUser = async (data) => {
    console.log(data);
    let user = await this.models.userModel.findOneAndUpdate(
      { _id: data?._id },
      {
        email: data?.email,
        phoneNumber: data?.phoneNumber,
      },
      { new: true }
    );
    await user.save();
    return user;
  };
}
export default userService;
