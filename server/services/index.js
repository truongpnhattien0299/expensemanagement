import userService from "./user.service.js";
const serviceLoader = (models) => {
  const users = new userService(models);
  return { users };
};
export default serviceLoader;
