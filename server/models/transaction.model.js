import mongoose from "mongoose";

const { Schema, model, Types } = mongoose;
const transactionSchema = new Schema(
  {
    user: {
      type: Types.ObjectId,
      ref: "User",
    },
    amount: {
      type: Number,
      required: true,
    },
  },
  { timestamps: true }
);

export const Transaction = model("Transaction", transactionSchema);
