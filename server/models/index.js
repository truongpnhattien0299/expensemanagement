import mongoose from "mongoose";
import userSchema from "./user.model.js";

const modelLoader = () => ({
  userModel: mongoose.model("User", userSchema),
});

export default modelLoader;
