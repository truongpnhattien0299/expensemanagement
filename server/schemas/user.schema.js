import { gql } from "apollo-server";

export default gql`
  type User {
    firstName: String
    lastName: String
    userName: String
    password: String
    email: String
    phoneNumber: String
  }
  type Query {
    users: [User]
  }

  input UserInput {
    firstName: String
    lastName: String
    email: String!
    phoneNumber: String!
  }
  type Mutation {
    createUser(input: UserInput): User
    updateUser(
      _id: ID!
      firstName: String
      lastName: String
      email: String!
      phoneNumber: String!
    ): User
  }
`;
